package com.beacon.server.functional;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * This class showcases functional programming paradigms and is not needed for the project
 * */
public class FunctionalExample {

    /**
     * this method is side effect free, since it will always return the same result
     * regardless of how often you call the method with the same input*/
    public int multipliedby2(int input){
        return input*2;
    }
    /**
     * this method covers final data structures and functions as parameters
     * */
    public List<String> splitAndFlatMap(final List<String>input){
        return input.stream()
                .map(string -> string.split(" "))
                .flatMap(Arrays::stream)
                .collect(Collectors.toList());
    }

    /**
     * this method covers higher order function, functions as parameters and return values and anonymous functions
     * */
    public void sortList(List<String> input){
        // Higher order functions since Collections.sort accepts a lambda method as its parameter
        Comparator<String> stringComparator = String::compareTo;
        // this method call would also return a function
        stringComparator.reversed();
        Collections.sort(input, stringComparator );
        // example for anonymous function
        Comparator<String> anonymousFunctionComparator = (String a, String b) -> {
            return b.compareTo(a);
        };
    }

    // example
    public static void main (String [] args){
        FunctionalExample functionalExample = new FunctionalExample();
        List<String> testList = Arrays.asList("i am here","you are there","and they are somewhere completely else");
        List<String> splittedAndFlatted = functionalExample.splitAndFlatMap(testList);
        splittedAndFlatted.forEach(System.out::println);
        functionalExample.sortList(splittedAndFlatted);
        splittedAndFlatted.forEach(System.out::println);
    }
}
