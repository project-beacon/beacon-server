package com.beacon.server.controller;

import com.beacon.server.entity.Event;
import com.beacon.server.entity.User;
import com.beacon.server.entity.UserEvent;
import com.beacon.server.payload.CreateEventPayload;
import com.beacon.server.payload.StopEventPayload;
import com.beacon.server.payload.UserEventPayload;
import com.beacon.server.payload.UserPayload;
import com.beacon.server.redis.RedisClient;
import com.beacon.server.redis.models.EventAttendanceModel;
import com.beacon.server.redis.models.UserEventInfo;
import com.beacon.server.repository.EventRepository;
import com.beacon.server.repository.UserEventRepository;
import com.beacon.server.repository.UserRepository;
import com.google.gson.Gson;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;
/**
 * This class handles all incoming requests which the backend can deal with
 * */
@Controller
@RequestMapping(path = "/beacon")
public class RequestController {

    /**
     * allows access to the user Table in mysql
     * */
    @Autowired
    private UserRepository userRepository;
    /**
     * allows access to the event table in mysql
     * */
    @Autowired
    private EventRepository eventRepository;
    /**
     * allows access to the user_event table in mysql
     * */
    @Autowired
    private UserEventRepository userEventRepository;
    /**
     * is the implementation for the redis cache during events
     * */
    @Autowired
    private RedisClient redisClient;

    /**
     * this method creates a new user in the database
     * @param userPayload is the payload which contains all the input from the user
     * @return statuscode 201 if success, otherwise statuscode 400
     */
    @RequestMapping(path = "/user", method = RequestMethod.POST)
    public @ResponseBody
    ResponseEntity<String> createUser(@RequestBody UserPayload userPayload) {
        User user = new User();
        if (!isValidUserPayload(userPayload)) {
            return ResponseEntity.badRequest().body("Your payload was not valid, user could not be created");
        }
        user.setEmail(userPayload.getEmail());
        user.setPassword(userPayload.getPassword().hashCode());
        userRepository.save(user);
        return ResponseEntity.status(201).body("User was created");
    }

    /**
     * this method is used to validate the login of a user
     * @param userPayload is the payload which contains all the input from the user
     * @return statuscode 201 if success otherwise return statuscode 420
     * */
    @RequestMapping(path = "/login", method = RequestMethod.POST)
    public ResponseEntity<String> loginUser(@RequestBody UserPayload userPayload) {
        Optional<User> user = userRepository.findById(userPayload.getEmail());
        AtomicBoolean success = new AtomicBoolean(false);
        user.ifPresent(u -> success.set(u.getPassword() == userPayload.getPassword().hashCode()));
        return success.get() ? ResponseEntity.status(201).body("Success") : ResponseEntity.status(420).body("The password does not match the user");
    }

    /**
     * this method is used to create an event
     * @param createEventPayload is the payload which contains all the input of the user regarding the event
     * @return statuscode 201 for created
     * */
    @RequestMapping(path = "/event", method = RequestMethod.POST)
    public ResponseEntity<String> createEvent(@RequestBody CreateEventPayload createEventPayload) {
        Event event = new Event();
        event.setCreationTime(createEventPayload.getCreationTime());
        event.setCreatorId(createEventPayload.getCreatorId());
        event.setName(createEventPayload.getName());
        event.setLatitude(createEventPayload.getLatitude());
        event.setLongitude(createEventPayload.getLongitude());
        eventRepository.save(event);
        redisClient.startEventAttendanceCount(event);
        Map<String, Object> responseMap = new HashMap<>();
        responseMap.put("eventId", event.getId());
        responseMap.put("eventName", event.getName());
        return ResponseEntity.status(201).body(new Gson().toJson(responseMap));
    }

    /**
     * this method stops an event in the sense that it will no longer log users active in the event in redis.
     * all users will be written to the user_event table and then the event will be deleted from redis
     * @param stopEventPayload contains the id of the event which should be stopped
     * @return statuscode 200 if successful otherwise return 404
     * */
    @RequestMapping(path = "/event", method = RequestMethod.DELETE)
    public ResponseEntity<String> stopEvent(@RequestBody StopEventPayload stopEventPayload) {
        Optional<Event> eventById = eventRepository.findById(stopEventPayload.getEventId());
        if (eventById.isPresent()) {
            EventAttendanceModel eventAttendanceModel = redisClient.getEventAttendanceModelForEvent(eventById.get());
            eventAttendanceModel.getUserEntriesMap()
                    .values()
                    .stream()
                    .map(userEntries -> createUserEvent(stopEventPayload.getEventId(), userEntries))
                    .forEach(this::saveUserEvent);
            redisClient.deleteEvent(eventById.get());
            return ResponseEntity.ok().body("Event has been stopped");
        }
        return ResponseEntity.notFound().build();
    }


    /**
     * this method updates the redis log for users for the specific event
     * @param userEventPayload contains the envent id and user information
     * @return statuscode 201
     * */
    @RequestMapping(path = "/update", method = RequestMethod.POST)
    public ResponseEntity<String> updateEvent(@RequestBody UserEventPayload userEventPayload) {
        Optional<Event> event = eventRepository.findById(userEventPayload.getEventId());
        event.ifPresent(e -> redisClient.addUserEventInfo(e, new UserEventInfo(userEventPayload.getTimeStamp(), userEventPayload.getUserId())));
        return ResponseEntity.accepted().body("Your request has been accepted");
    }

    /**
     * this method returns the number of people which are active in an event
     * @param eventId  is the id of the event
     * @return statuscode 200 if successful, otherwise 404
     * */
    @RequestMapping(value = "/event", method = RequestMethod.GET)
    public ResponseEntity<String> getEventAttendanceCount(@RequestParam("id") int eventId) {
        Optional<Event> event = eventRepository.findById(eventId);
        if (event.isPresent()) {
            EventAttendanceModel eventAttendanceModel = redisClient.getEventAttendanceModelForEvent(event.get());
            if (eventAttendanceModel != null) {
                int count = eventAttendanceModel.getUserEntriesMap().keySet().size();
                Map<String, Object> responseMap = new TreeMap<>();
                responseMap.put("eventId", event.get().getId());
                responseMap.put("numberOfAttendees", count);
                return ResponseEntity.ok().body(new Gson().toJson(responseMap));
            }
        }
        return ResponseEntity.notFound().build();
    }


    /**
     * this method creates a UserEvent Entity
     * @param eventId is the id of the Event
     * @param userEventInfos contains additional information regarding the user
     * @return {@link UserEvent}
     * */
    private UserEvent createUserEvent(int eventId, List<UserEventInfo> userEventInfos) {
        UserEvent userEvent = new UserEvent();
        userEvent.setUserId(userEventInfos.get(0).getUserID());
        userEvent.setTimeEntered(userEventInfos.get(0).getTimestamp());
        userEvent.setTimeLeft(userEventInfos.get(userEventInfos.size() - 1).getTimestamp());
        userEvent.setEventId(eventId);
        return userEvent;
    }

    /**
     * this method checks whether the userpayload has non empty fields
     * @param userPayload contains user information
     * @return true if all non empty else false
     * */
    private boolean isValidUserPayload(UserPayload userPayload) {
        return StringUtils.isNotBlank(userPayload.getPassword()) && StringUtils.isNotBlank(userPayload.getEmail());
    }

    /**
     * this method saves a {@link UserEvent} Entity to the database
     * @param userEvent is the UserEvent Entity which is to be saved
     * */
    private void saveUserEvent(UserEvent userEvent) {
        userEventRepository.save(userEvent);
    }
}
