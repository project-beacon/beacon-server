package com.beacon.server.dsl;

/**
 * This class is for a small dsl example with vector addition, subtraction and multiplication in 3 dimensions
 * */
public class Vec3 {
    /**
     * x coordinate
     * */
    private double x;
    /**
     * y coordinate
     * */
    private double y;
    /**
     * z coordinate
     * */
    private double z;


    private Vec3(double x, double y, double z){
        this.x = x;
        this.y = y;
        this.z = z;
    }

    /**
     * static accessor and starting point for vector
     * */
    public static Vec3 from(double x, double y, double z){
        return new Vec3(x,y,z);
    }

    /**
     * multiplies itself with the factor and returns the result as a new {@link Vec3}
     * */
    public Vec3 multiply(double factor){
        return new Vec3(x*factor,y*factor,z*factor);
    }

    /**
     * adds the other vector to itself and returns the result as a new {@link Vec3}
     * */
    public Vec3 add(Vec3 other){
        return new Vec3(x + other.x, y+other.y, z+other.z);
    }
    /**
     * subtracts the other vector from itself and returns the result as a new {@link Vec3}
     * */
    public Vec3 subtract(Vec3 other){
        return new Vec3(x-other.x,y - other.y, z - other.z);
    }

    /**
     * prints the current vector formatted
     * */
    public void printVector(){
        System.out.printf("[%s%n%s%n%s]",this.x,this.y,this.z);
    }

    // example
    public static void main (String[]args){
        from(3,1,5).multiply(3).add(from(3,2,1)).subtract(from(1,1,1)).printVector();

    }


}
