package com.beacon.server.payload;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * This class represents the incoming payload for stopping an event
 * */
public class StopEventPayload {


    private int eventId;

    /**
     * standard constructor which maps the fields to the incoming JSON
     * */
    @JsonCreator
    public StopEventPayload(@JsonProperty("eventId") int eventId) {
        this.eventId = eventId;
    }

    public int getEventId() {
        return eventId;
    }

    public void setEventId(int eventId) {
        this.eventId = eventId;
    }
}
