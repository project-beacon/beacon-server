package com.beacon.server.payload;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * This class represents the incoming payload for handling user related requests, such as creating, deleting and login
 * */
public class UserPayload {


    private String email;

    private String password;

    /**
     * standard constructor which maps the fields to the incoming JSON
     * */
    @JsonCreator
    public UserPayload(@JsonProperty("mail") String email, @JsonProperty("password") String password) {
        this.email = email;
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
