package com.beacon.server.payload;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * This class represents the incoming payload for updating user information during an event
 * */
public class UserEventPayload {

    /**
     * time at the moment of the request
     * */
    private long timeStamp;

    private String userId;

    private int eventId;

    /**
     * standard constructor which maps the fields to the incoming JSON
     * */
    @JsonCreator
    public UserEventPayload(@JsonProperty("timeStamp") long timeStamp, @JsonProperty("userId") String userId, @JsonProperty("eventId") int eventId) {
        this.timeStamp = timeStamp;
        this.userId = userId;
        this.eventId = eventId;
    }

    public long getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(long timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public int getEventId() {
        return eventId;
    }

    public void setEventId(int eventId) {
        this.eventId = eventId;
    }
}
