package com.beacon.server.payload;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * This class represents the incoming payload for creating an event
 * */
public class CreateEventPayload {

    /**
     * name of the event
     * */
    private String name;
    /**
     * id of the person who created the event
     * */
    private String creatorId;
    /**
     * longitude of where the event was created
     * */
    private double longitude;
    /**
     * latitude of where the event was created
     * */
    private double latitude;
    /**
     * time at which the event was created
     * */
    private long creationTime;

    /**
     * standard constructor which maps the fields to the incoming JSON
     * */
    @JsonCreator
    public CreateEventPayload(@JsonProperty("name") String name, @JsonProperty("creatorId") String creatorId, @JsonProperty("longitude") double longitude,
                              @JsonProperty("latitude") double latitude, @JsonProperty("creationTime") long creationTime) {
        this.name = name;
        this.creatorId = creatorId;
        this.longitude = longitude;
        this.latitude = latitude;
        this.creationTime = creationTime;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(String creatorId) {
        this.creatorId = creatorId;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public long getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(long creationTime) {
        this.creationTime = creationTime;
    }
}
