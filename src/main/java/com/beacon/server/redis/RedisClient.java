package com.beacon.server.redis;


import com.beacon.server.entity.Event;
import com.beacon.server.redis.models.EventAttendanceModel;
import com.beacon.server.redis.models.UserEventInfo;
import com.google.gson.Gson;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

import java.util.ArrayList;

/**
 * This class is used to access redis, using jedis*/
@Component
public class RedisClient {

    /**
     * gson is used to transform objects to json and back to their object form
     * */
    private Gson gson;
    /**
     * contains a pool of jedis connections, is used because it is threadsafe, as opposed to using only jedis
     * */
    private JedisPool jedisPool;
    /**
     * Logs information*/
    private static final Logger LOGGER = LoggerFactory.getLogger(RedisClient.class);

    @Autowired
    public RedisClient() {
        this.gson = new Gson();
        this.jedisPool = new JedisPool();
    }

    /**
     * This method is called after creating an event in the database and will also create it in redis.
     * In redis it is used to track how many are actually attending the event, hence not {@link Event} is stored
     * but {@link EventAttendanceModel}
     * @param event contains all information regarding the event*/
    public void startEventAttendanceCount(Event event) {

        EventAttendanceModel eventAttendanceModel = new EventAttendanceModel(event.getId());
        eventAttendanceModel.getUserEntriesMap().put(event.getCreatorId(), new ArrayList<>());
        String eventAttendaceModelJSON = gson.toJson(eventAttendanceModel);
        try (Jedis jedis = jedisPool.getResource()) {
            jedis.set(String.format("%s%s", event.getName(), event.getId()), eventAttendaceModelJSON);
            LOGGER.info("Event with the ID {} has started counting in redis", event.getId());
        } catch (Exception exception) {
            LOGGER.info("Event with the ID {} could not be created in redis", event.getId(), exception);
        }

    }

    /**
     * this method deletes the given event from redis
     * @param event which is to be deleted
     * */
    public void deleteEvent(Event event) {
        try (Jedis jedis = jedisPool.getResource()) {
            jedis.del(String.format("%s%s", event.getName(), event.getId()));
            LOGGER.info("Event with the ID {} was successfully deleted from redis", event.getId());
        } catch (Exception exception) {
            LOGGER.info("Event with the ID {} could not be be deleted from redis", event.getId(), exception);
        }

    }

    /**
     * this method adds{@link UserEventInfo} to the event in redis
     * If the user doesnt exist yet a new entry will be created for said user
     * @param event is the event to which {@link UserEventInfo} should be added
     * @param userEventInfo contains the user information
     * */
    public void addUserEventInfo(Event event, UserEventInfo userEventInfo) {
        try (Jedis jedis = jedisPool.getResource()) {
            String key = String.format("%s%s", event.getName(), event.getId());
            String eventAttendanceModelJSON = jedis.get(key);
            if (StringUtils.isNotBlank(eventAttendanceModelJSON)) {
                EventAttendanceModel eventAttendanceModel = gson.getAdapter(EventAttendanceModel.class).fromJson(eventAttendanceModelJSON);
                if (eventAttendanceModel.getUserEntriesMap().containsKey(userEventInfo.getUserID())) {
                    eventAttendanceModel.getUserEntriesMap().get(userEventInfo.getUserID()).add(userEventInfo);
                } else {
                    eventAttendanceModel.getUserEntriesMap().put(userEventInfo.getUserID(), new ArrayList<>());
                    eventAttendanceModel.getUserEntriesMap().get(userEventInfo.getUserID()).add(userEventInfo);
                }
                jedis.set(key, gson.toJson(eventAttendanceModel));
            }
        } catch (Exception exception) {
            LOGGER.info("User Info for userID {} could not be added", userEventInfo.getUserID(), exception);
        }
    }

    /**
     * this method returns the number of people participating in an event
     * @param event is the event which is to be checked
     * */
    public EventAttendanceModel getEventAttendanceModelForEvent(Event event){
        try (Jedis jedis = jedisPool.getResource()) {
            String eventAttendanceModelJSON = jedis.get(String.format("%s%s", event.getName(), event.getId()));
            if (StringUtils.isNotBlank(eventAttendanceModelJSON)) {
                EventAttendanceModel eventAttendanceModel = gson.getAdapter(EventAttendanceModel.class).fromJson(eventAttendanceModelJSON);
                return eventAttendanceModel;
            }
        } catch (Exception exception) {
            LOGGER.info("EventAttendanceModel for Event with the id {} could not be returned", event.getId(), exception);
        }
        return null;
    }

}
