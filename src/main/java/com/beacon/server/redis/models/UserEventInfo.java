package com.beacon.server.redis.models;

/**
 * This class represents the information which the user transmits periodically while he is participating in an event.
 * It contains the time of transmission and the user id
 * */
public class UserEventInfo {

    /**
     * time of transmission
     * */
    private long timestamp;

    private String userID;

    public UserEventInfo(long timestamp, String userID) {
        this.timestamp = timestamp;
        this.userID = userID;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public String getUserID() {
        return userID;
    }
}
