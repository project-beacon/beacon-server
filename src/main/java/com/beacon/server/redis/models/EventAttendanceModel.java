package com.beacon.server.redis.models;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * This class represents a container which contains all UserEventUnfo Entries for all users participating in the event
 * */
public class EventAttendanceModel {

    /**
     * id of the event
     * */
    private int eventId;
    /**
     * map which contains a List of UserInfo Entries for every User
     * */
    private Map<String, List<UserEventInfo>> userEntriesMap;

    public EventAttendanceModel(int eventId) {
        this.eventId = eventId;
        userEntriesMap = new HashMap<>();
    }

    public int getEventId() {
        return eventId;
    }

    public Map<String, List<UserEventInfo>> getUserEntriesMap() {
        return userEntriesMap;
    }
}
