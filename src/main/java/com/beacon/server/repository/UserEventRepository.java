package com.beacon.server.repository;

import com.beacon.server.entity.UserEvent;
import org.springframework.data.repository.CrudRepository;

/**
 * This interface will be autowired by Spring so that we can access the Event Table
 * */
public interface UserEventRepository extends CrudRepository<UserEvent,Integer> {
}
