package com.beacon.server.repository;

import com.beacon.server.entity.User;
import org.springframework.data.repository.CrudRepository;


/**
 * This interface will be autowired by Spring so that we can access the Event Table
 * */
public interface UserRepository extends CrudRepository<User, String> {

}
