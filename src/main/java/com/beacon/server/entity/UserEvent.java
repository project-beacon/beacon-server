package com.beacon.server.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * This class represents the UserEvent entity and will be created by hibernate in the mysql database
 * */
@Entity
public class UserEvent {

    /**
     * ID/PK Column
     * */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    /**
     * time at which the user first entered the event
     * */
    private long timeEntered;
    /**
     * time at which the user left the event
     * */
    private long timeLeft;

    private String userId;

    private int eventId;

    public long getTimeEntered() {
        return timeEntered;
    }

    public void setTimeEntered(long timeEntered) {
        this.timeEntered = timeEntered;
    }

    public long getTimeLeft() {
        return timeLeft;
    }

    public void setTimeLeft(long timeLeft) {
        this.timeLeft = timeLeft;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public int getEventId() {
        return eventId;
    }

    public void setEventId(int eventId) {
        this.eventId = eventId;
    }
}
