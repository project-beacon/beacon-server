package com.beacon.server.entity;

import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * This class represents the User entity and will be created by hibernate in the mysql database
 * */
@Entity
public class User {

	/**
	 * hashed password of the user
	 * */
    private int password;
    /**
	 * ID/PK column
	 * */
    @Id
    private String email;

	public int getPassword() {
		return password;
	}

	public void setPassword(int password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
    
    
}

