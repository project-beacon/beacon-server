package com.beacon.server;

import com.beacon.server.entity.Event;
import com.beacon.server.redis.RedisClient;
import com.beacon.server.redis.models.EventAttendanceModel;
import com.beacon.server.redis.models.UserEventInfo;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RedisClientTest {

    public static final String EVENT_CREATOR = "user@example.com";
    public static final String ADDED_USER_ID = "someguy@example.com";
    @Autowired
    RedisClient redisClient;
    Event event;
    @Before
    public void setUp(){
        Event testEvent = new Event();
        testEvent.setId(0);
        testEvent.setCreationTime(1243567);
        testEvent.setCreatorId(EVENT_CREATOR);
        testEvent.setLatitude(22.21);
        testEvent.setLongitude(64.11);
        testEvent.setName("testEventName");
        this.event = testEvent;
    }
    @Test
    public void testCreateEvent(){
        redisClient.startEventAttendanceCount(event);
        EventAttendanceModel eventAttendanceModel = redisClient.getEventAttendanceModelForEvent(event);
        Assert.assertNotNull(eventAttendanceModel);
        Assert.assertEquals(event.getId(),eventAttendanceModel.getEventId());
        Assert.assertEquals(0,eventAttendanceModel.getUserEntriesMap().get(event.getCreatorId()).size());
        redisClient.deleteEvent(event);
    }

    @Test
    public void testUpdateEvent(){
        redisClient.startEventAttendanceCount(event);
        UserEventInfo testUserEventInfo = new UserEventInfo(event.getCreationTime(),EVENT_CREATOR);
        UserEventInfo testUserEventInfo2 = new UserEventInfo(event.getCreationTime()+1234, ADDED_USER_ID);
        UserEventInfo testUserEventInfo3 = new UserEventInfo(testUserEventInfo2.getTimestamp()+1234, ADDED_USER_ID);

        redisClient.addUserEventInfo(event,testUserEventInfo);
        redisClient.addUserEventInfo(event, testUserEventInfo2);
        redisClient.addUserEventInfo(event, testUserEventInfo3);
        EventAttendanceModel eventAttendanceModel = redisClient.getEventAttendanceModelForEvent(event);

        Assert.assertNotNull(eventAttendanceModel);
        Assert.assertEquals(event.getId(),eventAttendanceModel.getEventId());
        Assert.assertEquals(1,eventAttendanceModel.getUserEntriesMap().get(EVENT_CREATOR).size());
        Assert.assertEquals(2,eventAttendanceModel.getUserEntriesMap().get(ADDED_USER_ID).size());

        redisClient.deleteEvent(event);
    }
}
