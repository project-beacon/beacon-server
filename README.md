# Beacon Server
**DISCLAIMER**: This is only the server part of the beacon application, not the app which the users will eventually use.

If you want to test it you will have to use a programm like **Postman** to send Requests or do it via **cURL**


To run the server part of the beacon application you will need to fulfill some prerequisites:
* run mysql ( you can use the docker compose which is in the docker package in this projekt, as that is preconfigured )
* run redis on the machine where you run this application
    * if you use a docker image make sure you expose the port 6379 for the host machine, otherwise you will need to do changes to the code

If you have these you can simply start by changing into the project directory and run **mvn sprint-boot:run**

## 1. UML

### 1.1 Sequence Diagramm

![here should be the sequence diagramm](docs/images/SequenceDiagram.jpg)

## 1.2 State Diagramm 

![here should be the state diagramm](docs/images/state_diagramm.jpg)

## 1.3 Use Case Diagramm

![here should be the use case diagramm](docs/images/UseCaseDiagramm.png)

## 2. Metrics

### SonarQube/Testing/Comments

For SonarQube i used a Docker container which you can also use, as the docker-compose is in the docker package in this project.
The sonar analysis is actually a step of the build pipeline
* should the sonar analysis fail ( quality gate ) then the project will not be built

You cann see the the result of the sonar analysis here:
![here should be sonar](docs/images/sonar.png)
The code is deployed onto the SonarQube Server using maven, which you will see in the CI part

For testing purposes JUnit was used and variables and meaningful methods all have comments
* simple getter and setter do not have comments


## 3. Build Tool

As a Build Tool maven was used.

It is used specifically in the Pipeline to run the test at one stage, deploy to sonar in the other and build the project in the last.

Furthermore it is used to manage the dependencies of the application

## 4. Clean code

### 4.1 Use of Comments

Comments are used to describe specifically what methods do, what their parameters do and what they will return.

They also describe the use of fields/member variables

They are not used to describe mundane, unimportant stuff such as getters and setters where there is no comment necessary or self explanatory variables


### 4.2 Organized

Files are well structured in their respective packages. That way you can find the kind of class you need faster, should you be in need of that.
* e.g. Entity Classes are kept in the entity package


The classes themselves are also structured well with imports, fields and methods.
For readability the maximum number of LOC is also kept very low and every method is below the threshold for cyclomatic complexity
### 4.3 Meaningful variable and method names

Instead of having magical numbers and variables/methods which are not self explanatory, all variables and methods are named in a way to ease your way working into the project as a new person

e.g. :
* the variable **redisClient** already speaks for itself what it is
* the method *createUser* already describes what it does specifically

### 4.4 Constraints

Besides copy and paste, return null is one of the most common programming errors. The
problem with empty and unset return values is that these empty objects are not easily
traced back and can be passed up.

To remedy that in certain places there will be checks in place.

In this case it will be checked whether the UserPayload is valid and if not the creation of the user will be rejected with the proper statuscode
```java
if (!isValidUserPayload(userPayload)) {
            return ResponseEntity.badRequest().body("Your payload was not valid, user could not be created");
        }

```

### 4.5 Reduce the cognitive load and nested conditions

Code is read 5-8 more than it is written.

In order to easen the way into new projects for new developers we also use SonarQube's metrics
* SonarQube allows you to track the cognitive complexity for each method you write
* it also allows you to check whether you have to many conditions in one method
    * conditions are kept to <= 3 per method


## 5. CI/CD

For continuous delivery i decided to use jenkins.

I installed jenkins onto my machine and created a Jenkinsfile for my project.
* In a Jenkins file you can declare what a jenkins pipeline will do for each stage you define
    * you can define as many stages as you want to
```java
pipeline {
    agent any
    tools {
        maven 'maven3'
        jdk 'JDK 8'
    }
    stages {
        stage ('Initialize') {
            steps {
                sh '''
                    echo "PATH = ${PATH}"
                    echo "M2_HOME = ${M2_HOME}"
                '''
            }
        }

        stage('Tests') {
            steps {
                sh 'mvn test'
            }
        }

        stage('Sonar Analysis') {
            steps {
                sh 'mvn sonar:sonar'
            }
        }
        
        
        stage('mvn clean install') {
            steps {
                sh 'mvn clean install -DskipTests=true'
            }
        }

    }

}
```

Here you can see the pipeline on my local machine:
![here should be jenkins pipeline](docs/images/jenkins_pipe.png)

And here you can see that it checks out this git repository when building:
![here should be jenkins console](docs/images/jenkins_console.png)

## 6. DSL
For a DSL i decided to do a 3 dimensional Vector which has the operations *add*, *multiply* and *subtract*
* A DSL focuses on a small area but is very descriptive in that area

Here you can see a small code Snippet of *Vec3* ( you can find it in the source code in the dsl package )
```java
from(3,1,5).multiply(3).add(from(3,2,1)).subtract(from(1,1,1)).printVector();
```

## 7. Functional Programming

You can find the following points in the class FunctionalExample which is located in the functional package in the project.
* only final data structures
* (mostly) side effect free functions
* the use of higher order functions
* functions as parameters and return values
* use clojures / anonymous functions
